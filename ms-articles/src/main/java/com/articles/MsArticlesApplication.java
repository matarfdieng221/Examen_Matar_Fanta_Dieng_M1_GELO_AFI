package com.articles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsArticlesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsArticlesApplication.class, args);
	}

}
