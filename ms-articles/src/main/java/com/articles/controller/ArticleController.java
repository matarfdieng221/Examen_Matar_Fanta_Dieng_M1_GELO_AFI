package com.articles.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.articles.entity.Article;
import com.articles.service.ArticleService;

@RestController
@RequestMapping("/api/articles")
public class ArticleController {

	@Autowired
	ArticleService articleService;
	
	//création d'un mappage get qui récupère tous les détails des articles de la base de données
	
	@GetMapping("/article")  
	private List<Article> getAllArticles()   
	{  
	return articleService.getAllArticles();  
	}  
	

	// création d'un mappage get qui récupère le détail d'un article spécifique
	
	@GetMapping("/article/{id}")  
	private Article getAticles(@PathVariable("id") int id)   
	{  
	return articleService.getArticlesById(id);  
	}  
	
	// création d'un mappage de suppression qui supprime un article spécifié
	@DeleteMapping("/article/{id}")  
	private String deleteArticle(@PathVariable("id") int id)   
	{  
	articleService.delete(id);
	return"id : "+id+" est supprimé avec succes";
	}  
	
	//insertion
	
	@PostMapping("/articles")  
	private Article saveArticle(@RequestBody Article article)   
	{  
	articleService.saveOrUpdate(article);  
	return article;  
	}  
	
	// mettre a jour
	
	@PutMapping("/articles")  
	private Article update(@RequestBody Article article)   
	{  
	articleService.saveOrUpdate(article);  
	return article;  
	}  
	
}
