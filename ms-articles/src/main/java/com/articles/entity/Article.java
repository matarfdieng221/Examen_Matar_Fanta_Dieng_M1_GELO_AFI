package com.articles.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_article")
public class Article {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String nom;
	private int prix_unitaire;
	private int quantite;
	public Article() {
		super();
	}
	public Article(String nom, int prix_unitaire, int quantite) {
		super();
		this.nom = nom;
		this.prix_unitaire = prix_unitaire;
		this.quantite = quantite;
	}
	public Article(int id, String nom, int prix_unitaire, int quantite) {
		super();
		this.id = id;
		this.nom = nom;
		this.prix_unitaire = prix_unitaire;
		this.quantite = quantite;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getPrix_unitaire() {
		return prix_unitaire;
	}
	public void setPrix_unitaire(int prix_unitaire) {
		this.prix_unitaire = prix_unitaire;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	
	
	
}
