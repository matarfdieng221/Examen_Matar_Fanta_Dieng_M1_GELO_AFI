package com.articles.reposiroty;

import org.springframework.data.jpa.repository.JpaRepository;

import com.articles.entity.Article;

public interface ArticleRepository extends JpaRepository<Article, Integer>{

}
